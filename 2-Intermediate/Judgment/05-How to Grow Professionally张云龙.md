# 如何培养自己的专业技能

承担责任超出了你的权威。你想要扮演的角色。更大的成功组织表达他对人民的贡献以及帮助你个人的事情

如果你想成为一个团队的领导者，就要煽动他们与你达成共识。如果你想成为一名主管，就把责任列入日程表。你可以轻松的与领导或是主管一起工作，因为这使他们承担了更大的责任。如果这有很多的困难，就一次做一点

如果你要成为一个更好的程序员就要自我评价，向你钦佩的人请教如何才能成为他们那样。你也可以向你的老板请教，他可能会告诉你的很少但这对你的职业生涯很重要。

做出规划去学习一些新技能，琐碎的技术，喜欢学习新的软件系统和认识艰难的社会，良好的编程习惯，把他们整合到你的工作中去。

下一节 【如何评估一个面试者】（06-如何评估一个面试者.md）