# 如何做出购买和建造的决定 

创业公司或项目想用完成一些与软件必须不断地做出所谓的购买和建造的决定。在这两方面的措辞是不幸的：似乎忽略了开源和免费的软件不一定是买的。更为重要的是他或许被称做“获取和整合与构建和集成”的决定因为这必须要考虑综合的花费。这需要一个伟大的组合的业务管理和精明的工程。

- 你如何设计匹配的需求？
- 哪一个部分是你需要买的？
- 什么是评估集成的成本？
- 什么是集成的成本？
- 什么是评估集成的成本？
- 购买长期维护的成本是增加还是减少？
- 当建立它时你想不想被放入经营者的地位

你应该三思而后行建筑是大到足以作为一个完善的其他业务的基础。提出这些想法的通常是明亮和乐观的人，他们将为你的团队做出很大的贡献。如果他们的想法是引人注目的你可能要改变你的商务计划；但不要没有意识的思考就投资比你的商业还要大的方案。

之后考虑这些问题，你或许应该准备起草两份工程计划，一份用来建造一份用来卖。这将关注你考虑的集成成本。你也应该考虑两种方案长期维护的成本。通过评估集成成本，在你买软件之前你将必须做一个全面的评价。如果你不能评价它，买它时你将承担一种非理智的风险并且你应该决定购买相似的产品。如果有几种购买策略考虑，一些精力就不得不分配到每个策略评估上。

下一节【如何发展专业】（05-如何发展专业.md）